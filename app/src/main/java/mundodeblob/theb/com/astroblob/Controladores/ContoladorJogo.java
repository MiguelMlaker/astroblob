package mundodeblob.theb.com.astroblob.Controladores;


import android.content.res.Resources;

import mundodeblob.theb.com.astroblob.Modelos.ImagemComponente;
import mundodeblob.theb.com.astroblob.R;

public class ContoladorJogo {

    private ImagemComponente imgSup;
    private ImagemComponente imgCent;
    private ImagemComponente imgInf;
    private int completa;
    private int gif;
    private int corFundo;
    Resources resources;



    public ContoladorJogo(ImagemComponente imgSup, ImagemComponente imgCent, ImagemComponente imgInf, Resources resources) {
        this.imgSup = imgSup;
        this.imgCent = imgCent;
        this.imgInf = imgInf;
        this.resources = resources;
    }

    public boolean vefificarPosicoes() {
        boolean retorno = false;
        if (imgSup.getId() == imgCent.getId() && imgSup.getId() == imgInf.getId()) {
            switch (imgSup.getImagem()) {
                case (R.drawable.sol_pt0):
                    completa = R.drawable.sol_completo;
                    gif = R.drawable.sol_gif;
                    corFundo =  resources.getColor(R.color.sol_fundo);
                    retorno = true;
                    break;
                case (R.drawable.mercurio_pt0):
                    gif = R.drawable.mercurio_gif;
                    completa = R.drawable.mercurio_completo;
                    corFundo =  resources.getColor(R.color.mercurio_fundo);
                    retorno = true;
                    break;
                case (R.drawable.venus_pt0):
                    gif = R.drawable.venus_gif;
                    completa = R.drawable.venus_completo;
                    corFundo =  resources.getColor(R.color.venus_fundo);
                    retorno = true;
                    break;
                case (R.drawable.terra_pt0):
                    gif = R.drawable.terra_gif;
                    completa = R.drawable.terra_completo;
                    corFundo =  resources.getColor(R.color.terra_fundo);
                    retorno = true;
                    break;
                case (R.drawable.lua_terra_pt0):
                    gif = R.drawable.lua_terra_gif;
                    completa = R.drawable.lua_terra_completo;
                    corFundo =  resources.getColor(R.color.lua_terra_fundo);
                    retorno = true;
                    break;
                case (R.drawable.marte_pt0):
                    gif = R.drawable.marte_gif;
                    completa = R.drawable.marte_completo;
                    corFundo =  resources.getColor(R.color.marte_fundo);
                    retorno = true;
                    break;
                case (R.drawable.jupiter_pt0):
                    gif = R.drawable.jupiter_gif;
                    completa = R.drawable.jupiter_completo;
                    corFundo =  resources.getColor(R.color.jupiter_fundo);
                    retorno = true;
                    break;
                case (R.drawable.saturno_pt0):
                    gif = R.drawable.saturno_gif;
                    completa = R.drawable.saturno_completo;
                    corFundo =  resources.getColor(R.color.saturno_fundo);
                    retorno = true;
                    break;
                case (R.drawable.urano_pt0):
                    gif = R.drawable.urano_gif;
                    completa = R.drawable.urano_completo;
                    corFundo =  resources.getColor(R.color.urano_fundo);
                    retorno = true;
                    break;
                case (R.drawable.netuno_pt0):
                    gif = R.drawable.netuno_gif;
                    completa = R.drawable.netuno_completo;
                    corFundo =  resources.getColor(R.color.netuno_fundo);
                    retorno = true;
                    break;
                case (R.drawable.plutao_pt0):
                    gif = R.drawable.plutao_gif;
                    completa = R.drawable.plutao_completo;
                    corFundo =  resources.getColor(R.color.plutao_fundo);
                    retorno = true;
                    break;
            }
        }

        return retorno;
    }

     public boolean verificarDuasPosicoes (){

      	return (imgSup.getId() == imgCent.getId() || imgSup.getId()== imgInf.getId() || 
                 imgCent.getId() == imgInf.getId()); 
     }

    public int getCompleta() {
        return completa;
    }

    public ImagemComponente getImgSup() {
        return imgSup;
    }

    public void setImgSup(ImagemComponente imgSup) {
        this.imgSup = imgSup;
    }

    public ImagemComponente getImgCent() {
        return imgCent;
    }

    public void setImgCent(ImagemComponente imgCent) {
        this.imgCent = imgCent;
    }

    public ImagemComponente getImgInf() {
        return imgInf;
    }

    public void setImgInf(ImagemComponente imgInf) {
        this.imgInf = imgInf;
    }

    public void setCompleta(int completa) {
        this.completa = completa;
    }

    public int getGif() {
        return gif;
    }

    public void setGif(int gif) {
        this.gif = gif;
    }

    public int getCorFundo() {
        return corFundo;
    }

    public void setCorFundo(int corFundo) {
        this.corFundo = corFundo;
    }
}


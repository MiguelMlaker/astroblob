package mundodeblob.theb.com.astroblob.Threads;

import android.graphics.Color;
import android.widget.TextView;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Random;
import java.util.function.Function;

public class MudarCorTextView extends Thread {
    private ArrayList<TextView> texos;
    private Random c;

    public MudarCorTextView(ArrayList<TextView> texos) {
        this.texos = texos;
    }

    public void run() {
        while (true) {

            for (TextView text : texos) {
                mudarAleatorio(text);
                try {
                    sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void mudarAleatorio(TextView texto) {
        Random c = new Random();
        int r, g, b;
        r = c.nextInt(256);
        g = c.nextInt(256);
        b = c.nextInt(256);
        texto.setTextColor(Color.rgb(r, g, b));
    }

}

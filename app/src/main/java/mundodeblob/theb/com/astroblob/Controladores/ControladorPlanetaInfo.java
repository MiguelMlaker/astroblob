package mundodeblob.theb.com.astroblob.Controladores;


import android.content.Context;

import java.io.Serializable;

import mundodeblob.theb.com.astroblob.R;

public class ControladorPlanetaInfo implements Serializable {

    /*
       nomeAstro
       primeroParagrafo
       segundoParagrafo
       terceiroParagrafo
  */
    private String textos[];
    /*
       primeiraImagem
        segundaImagem
        terceiraImagem
        quartaImagem
   */

    private int imagens[];


    public void retornarPlanetaInfo(int imagemPlaneta, Context context) {

        String nomeAstro = "";
        int imagemTexto = 0;
        int primeiraImagem = 0;
        int segundaImagem = 0;
        int terceiraImagem = 0;
        int quartaImagem = 0;

        switch (imagemPlaneta) {

            case R.drawable.sol_completo:
                nomeAstro = context.getString(R.string.astro_nome_Sol);
                imagemTexto = R.drawable.sol_texto;
                primeiraImagem = R.drawable.sol_0;
                segundaImagem = R.drawable.sol_1;
                terceiraImagem = R.drawable.sol_2;
                quartaImagem = R.drawable.sol_3;
                break;
            case R.drawable.mercurio_completo:
                nomeAstro = context.getString(R.string.astro_nome_Mercurio);
                imagemTexto = R.drawable.mercurio_texto;
                primeiraImagem = R.drawable.mercurio_0;
                segundaImagem = R.drawable.mercurio_1;
                terceiraImagem = R.drawable.mercurio_2;
                quartaImagem = R.drawable.mercurio_3;
                break;
            case R.drawable.venus_completo:
                nomeAstro = context.getString(R.string.astro_nome_Venus);
                imagemTexto = R.drawable.venus_texto;
                primeiraImagem = R.drawable.venus_0;
                segundaImagem = R.drawable.venus_1;
                terceiraImagem = R.drawable.venus_2;
                quartaImagem = R.drawable.venus_3;
                break;
            case R.drawable.terra_completo:
                nomeAstro = context.getString(R.string.astro_nome_Terra);
                imagemTexto = R.drawable.terra_texto;
                primeiraImagem = R.drawable.terra_0;
                segundaImagem = R.drawable.terra_1;
                terceiraImagem = R.drawable.terra_2;
                quartaImagem = R.drawable.terra_3;
                break;
            case R.drawable.marte_completo:
                nomeAstro = context.getString(R.string.astro_nome_Marte);
                imagemTexto = R.drawable.marte_texto;
                primeiraImagem = R.drawable.marte_0;
                segundaImagem = R.drawable.marte_1;
                terceiraImagem = R.drawable.marte_2;
                quartaImagem = R.drawable.marte_3;
                break;
            case R.drawable.jupiter_completo:
                nomeAstro = context.getString(R.string.astro_nome_Jupiter);
                imagemTexto = R.drawable.jupiter_texto;
                primeiraImagem = R.drawable.jupiter_0;
                segundaImagem = R.drawable.jupiter_1;
                terceiraImagem = R.drawable.jupiter_2;
                quartaImagem = R.drawable.jupiter_3;
                break;
            case R.drawable.saturno_completo:
                nomeAstro = context.getString(R.string.astro_nome_Saturno);
                imagemTexto = R.drawable.saturno_texto;
                primeiraImagem = R.drawable.saturno_0;
                segundaImagem = R.drawable.saturno_1;
                terceiraImagem = R.drawable.saturno_2;
                quartaImagem = R.drawable.saturno_3;
                break;
            case R.drawable.urano_completo:
                nomeAstro = context.getString(R.string.astro_nome_Urano);
                imagemTexto = R.drawable.urano_texto;
                primeiraImagem = R.drawable.urano_0;
                segundaImagem = R.drawable.urano_1;
                terceiraImagem = R.drawable.urano_2;
                quartaImagem = R.drawable.urano_3;
                break;
            case R.drawable.netuno_completo:
                nomeAstro = context.getString(R.string.astro_nome_Netuno);
                imagemTexto = R.drawable.netuno_texto;
                primeiraImagem = R.drawable.netuno_0;
                segundaImagem = R.drawable.netuno_1;
                terceiraImagem = R.drawable.netuno_2;
                quartaImagem = R.drawable.netuno_3;
                break;
            case R.drawable.lua_terra_completo:
                nomeAstro = context.getString(R.string.astro_nome_Lua);
                imagemTexto = R.drawable.lua_texto;
                primeiraImagem = R.drawable.lua_0;
                segundaImagem = R.drawable.lua_1;
                terceiraImagem = R.drawable.lua_2;
                quartaImagem = R.drawable.lua_3;
                break;
            case R.drawable.plutao_completo:
                nomeAstro = context.getString(R.string.astro_nome_Plutao);
                imagemTexto = R.drawable.plutao_texto;
                primeiraImagem = R.drawable.plutao_0;
                segundaImagem = R.drawable.plutao_1;
                terceiraImagem = R.drawable.plutao_2;
                quartaImagem = R.drawable.plutao_3;
        }

        String textos[] = {nomeAstro};
        int imagens[] = {primeiraImagem, segundaImagem, terceiraImagem, quartaImagem, imagemTexto};

        this.textos = textos;
        this.imagens = imagens;

    }

    public String[] getTextos() {
        return textos;
    }

    public void setTextos(String[] textos) {
        this.textos = textos;
    }

    public int[] getImagens() {
        return imagens;
    }

    public void setImagens(int[] imagens) {
        this.imagens = imagens;
    }


}

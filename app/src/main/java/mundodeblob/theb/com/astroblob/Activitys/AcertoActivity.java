package mundodeblob.theb.com.astroblob.Activitys;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.os.Build;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.transition.Fade;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import mundodeblob.theb.com.astroblob.Services.BackgroundSoundServiceAcertoActivity;
import mundodeblob.theb.com.astroblob.Controladores.ControladorPlanetaInfo;
import mundodeblob.theb.com.astroblob.Services.BackgroundSoundServiceAcertoActivity2;
import mundodeblob.theb.com.astroblob.Threads.LampadaAnimacao;
import mundodeblob.theb.com.astroblob.Threads.MudarCorTextView;
import mundodeblob.theb.com.astroblob.R;


public class AcertoActivity extends AppCompatActivity {
    private ImageView img;
    private ConstraintLayout fundoAcerto;
    private ImageButton lampada;
    private ImageView botaoVoltarAcerto;
    private TextView textjogarNovamenteAcerto;
    private TextView textViewAcerto;
    private ControladorPlanetaInfo cpinfo;
    private int delay;
    private boolean gifSound;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_acerto);
        img = findViewById(R.id.img);

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        gifSound = false;

        //tela de transiçao
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Fade entradaS = new Fade();
            entradaS.setDuration(100);
            getWindow().setEnterTransition(entradaS);
            getWindow().setExitTransition(entradaS);
        }

        fundoAcerto = findViewById(R.id.fundoAcerto);
        lampada = findViewById(R.id.lampada);
        botaoVoltarAcerto = findViewById(R.id.botaoVoltarAcerto);
        textjogarNovamenteAcerto = findViewById(R.id.textjogarNovamenteAcerto);
        textViewAcerto = findViewById(R.id.textViewAcerto);

        new LampadaAnimacao(this, lampada).start();

        final Intent intent = getIntent();
        final int completa = intent.getIntExtra("completa", 0);
        final int corFundo = intent.getIntExtra("corFundo", Color.BLACK);
        final int gif = intent.getIntExtra("gif", 0);

        cpinfo = new ControladorPlanetaInfo();
        cpinfo.retornarPlanetaInfo(completa, getApplicationContext());

        Glide.with(getApplicationContext()).asGif().load(gif).into(img);
        Glide.with(getApplicationContext()).asGif().load(R.drawable.planeta_gif).into(botaoVoltarAcerto);

        String textos[] = cpinfo.getTextos();
        textViewAcerto.setText(textos[0].toUpperCase());

        ArrayList<TextView> textViews = new ArrayList<>();
        textViews.add(this.textjogarNovamenteAcerto);
        textViews.add(this.textViewAcerto);

        MudarCorTextView mct = new MudarCorTextView(textViews);
        mct.start();


        delay = 5600;

        img.postDelayed(new Runnable() {
            public void run() {
                //img.setImageResource(completa);
                Glide.with(getApplicationContext()).asDrawable().load(completa).into(img);
                gifSound = true;
            }
        }, delay);

        fundoAcerto.postDelayed(new Runnable() {
            public void run() {
                fundoAcerto.setBackgroundColor(corFundo);
            }
        }, delay);


        lampada.postDelayed(new Runnable() {
            public void run() {
                lampada.setVisibility(View.VISIBLE);
            }
        }, delay);

        botaoVoltarAcerto.postDelayed(new Runnable() {
            public void run() {
                botaoVoltarAcerto.setVisibility(View.VISIBLE);
            }
        }, delay);

        textjogarNovamenteAcerto.postDelayed(new Runnable() {
            @Override
            public void run() {
                textjogarNovamenteAcerto.setVisibility(View.VISIBLE);
            }
        }, delay);

        lampada.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent1 = new Intent(getApplicationContext(), PlanetaInfoActivity.class);
                intent1.putExtra("info", cpinfo);
                startActivity(intent1);
                stopService(new Intent(AcertoActivity.this, LampadaAnimacao.class));
            }
        });


        botaoVoltarAcerto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent1 = new Intent(getApplicationContext(), TelaJogoActivity.class);
                intent1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                stopService(new Intent(AcertoActivity.this, LampadaAnimacao.class));
                startActivity(intent1);
            }
        });

    }

    protected void onDestroy() {
        super.onDestroy();
            stopService(new Intent(this, BackgroundSoundServiceAcertoActivity2.class));
            stopService(new Intent(this, BackgroundSoundServiceAcertoActivity.class));
    }

    protected void onPause() {
        super.onPause();
            stopService(new Intent(this, BackgroundSoundServiceAcertoActivity2.class));
            stopService(new Intent(this, BackgroundSoundServiceAcertoActivity.class));

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (gifSound) {
            startService(new Intent(this, BackgroundSoundServiceAcertoActivity2.class));
        }else{
            startService(new Intent(this, BackgroundSoundServiceAcertoActivity.class));
        }
    }
}




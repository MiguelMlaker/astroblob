package mundodeblob.theb.com.astroblob.Threads;

import android.content.Context;
import android.widget.ImageButton;

import mundodeblob.theb.com.astroblob.R;

public class LampadaAnimacao extends Thread {

    private Context context;
    private ImageButton img;

    public LampadaAnimacao(Context context, ImageButton img) {
        this.context = context;
        this.img = img;
    }

    public void run() {
        int tempo = 400;
        while (true){
            try {
                this.img.setBackgroundResource(R.drawable.lamp_yellow);
                sleep(tempo);
                this.img.setBackgroundResource(R.drawable.lamp_gray);
                sleep(tempo);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

    }
}

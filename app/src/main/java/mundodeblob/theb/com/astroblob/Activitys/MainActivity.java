package mundodeblob.theb.com.astroblob.Activitys;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import java.util.ArrayList;
import mundodeblob.theb.com.astroblob.Services.BackgroundSoundServiceMainActivity;
import mundodeblob.theb.com.astroblob.Threads.MudarCorButton;
import mundodeblob.theb.com.astroblob.Threads.MudarCorTextView;
import mundodeblob.theb.com.astroblob.R;

public class MainActivity extends AppCompatActivity {

    //componentes
    private TextView titulo;
    private TextView titulo2;
    private Button play;
    private MediaPlayer soundButtonPlay;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Faz com que a tela da aplicação rode apenas no modo LANDSCAPE
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        //inicia os componentes
        titulo = findViewById(R.id.titulo);
        titulo2 = findViewById(R.id.titulo2);
        play = findViewById(R.id.play);

        //som do botão
        soundButtonPlay = MediaPlayer.create(this, R.raw.botao_sound_2);

        //inicia a thread
        mudarCor();

        //inicia a TelaJogoActivity com animação de som
        play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), TelaJogoActivity.class);
                startActivity(intent);
                soundButtonPlay.start();
            }
        });
    }

    //método que inicia thread para animação da cor dos textos
    public void mudarCor() {
        ArrayList<TextView> textos = new ArrayList<TextView>();
        textos.add(this.titulo);
        textos.add(titulo2);
        MudarCorTextView mc = new MudarCorTextView(textos);
        mc.start();
        MudarCorButton mcb = new MudarCorButton(play);
        mcb.start();
    }

    //inicia o servico de som de fundo
    @Override
    protected void onResume() {
        super.onResume();
        startService(new Intent(this, BackgroundSoundServiceMainActivity.class));
    }

    //para o servico de som de fundo
    protected void onDestroy() {
        super.onDestroy();
        stopService(new Intent(this, BackgroundSoundServiceMainActivity.class));
    }

    //pausa o servico de som de fundo
    protected void onPause() {
        super.onPause();
        stopService(new Intent(this, BackgroundSoundServiceMainActivity.class));
    }

}

package mundodeblob.theb.com.astroblob.Modelos;

public class ImagemComponente {

    private int Id;
    private int Imagem;
    private int contador;

    public int getContador() {
        return contador;
    }

    public void setContador(int contador) {
        this.contador = contador;
    }

    public ImagemComponente(int id, int imagem) {
        Id = id;
        Imagem = imagem;
        contador = 0;
    }


    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public int getImagem() {
        return Imagem;
    }

    public void setImagem(int imagem) {
        Imagem = imagem;
    }


    public void addContador(){
        this.contador++;
    }

    public void removeContador(){
        this.contador--;
    }
}

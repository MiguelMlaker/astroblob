package mundodeblob.theb.com.astroblob.Threads;

import android.graphics.Color;
import android.widget.Button;

import java.util.Random;

public class MudarCorButton extends Thread {
    private Button texto;
    private Random c;

    public MudarCorButton(Button texto) {
        this.texto = texto;
    }

    public void run() {
        while (true) {

                mudarAleatorio(texto);
                try {
                    sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }

    public void mudarAleatorio(Button texto) {
        Random c = new Random();
        int r, g, b;
        r = c.nextInt(256);
        g = c.nextInt(256);
        b = c.nextInt(256);
        texto.setTextColor(Color.rgb(r, g, b));
    }

}

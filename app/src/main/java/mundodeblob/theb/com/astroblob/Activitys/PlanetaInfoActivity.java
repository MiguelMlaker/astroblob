package mundodeblob.theb.com.astroblob.Activitys;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import mundodeblob.theb.com.astroblob.Controladores.ControladorPlanetaInfo;
import mundodeblob.theb.com.astroblob.R;

public class PlanetaInfoActivity extends AppCompatActivity {


    private TextView titulo_info_0;
    private TextView titulo_info_1;
    private ImageView imagemViewInfo_0;
    private ImageView imagemViewInfo_1;
    private ImageView imagemViewInfo_2;
    private ImageView imagemViewInfo_3;
    private  ImageView webViewPlanetaInfo;
    private ScrollView scrollViewPlanetaInfo;
    private FloatingActionButton floatingActionButtonInfo;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_planeta_info);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);


        final Intent intent = getIntent();
        ControladorPlanetaInfo cpinfo  = (ControladorPlanetaInfo)intent.getSerializableExtra("info");

        floatingActionButtonInfo = findViewById(R.id.floatingActionButtonInfo);

        titulo_info_0 = findViewById(R.id.titulo_info_0);
        titulo_info_1 = findViewById(R.id.titulo_info_1);

        imagemViewInfo_0 = findViewById(R.id.imagem_info_0);
        imagemViewInfo_1 = findViewById(R.id.imagem_info_1);
        imagemViewInfo_2 = findViewById(R.id.imagem_info_2);
        imagemViewInfo_3 = findViewById(R.id.imagem_info_3);

        webViewPlanetaInfo = findViewById(R.id.webViewPlanetaInfo);
        scrollViewPlanetaInfo = findViewById(R.id.scrollViewPlanetaInfo);

        String textos[] =  cpinfo.getTextos();
        int imagens[] = cpinfo.getImagens();

        titulo_info_0.setText("TUDO SOBRE " + textos[0].toUpperCase());
        titulo_info_1.setText("COMO " + textos[0].toUpperCase() + " SE PARECE?");

        Glide.with(getApplicationContext()).asDrawable().load(imagens[0]).into(imagemViewInfo_0);
        Glide.with(getApplicationContext()).asDrawable().load(imagens[1]).into(imagemViewInfo_1);
        Glide.with(getApplicationContext()).asDrawable().load(imagens[2]).into(imagemViewInfo_2);
        Glide.with(getApplicationContext()).asDrawable().load(imagens[3]).into(imagemViewInfo_3);
        Glide.with(getApplicationContext()).asDrawable().load(imagens[4]).into(webViewPlanetaInfo);

        //webViewPlanetaInfo.setFocusable(false);

        floatingActionButtonInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();


            }
        });

    }
}

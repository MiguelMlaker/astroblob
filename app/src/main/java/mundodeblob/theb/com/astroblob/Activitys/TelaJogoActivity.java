package mundodeblob.theb.com.astroblob.Activitys;

import android.app.ActivityOptions;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.media.MediaPlayer;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.transition.Fade;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.Collections;

import mundodeblob.theb.com.astroblob.Services.BackgroundSoundServiceAcertoActivity;
import mundodeblob.theb.com.astroblob.Services.BackgroundSoundServiceTelaJogo;
import mundodeblob.theb.com.astroblob.Controladores.ContoladorJogo;
import mundodeblob.theb.com.astroblob.Modelos.ImagemComponente;
import mundodeblob.theb.com.astroblob.R;


public class TelaJogoActivity extends AppCompatActivity {

    //componentes superiores
    private ImageView superior;
    private Button botaoSupEsq;
    private Button botaoSupDir;
    private ArrayList<ImagemComponente> superiores;
    private ImagemComponente atualSuperior;

    //componentes centrais
    private ImageView central;
    private Button botaoCentEsq;
    private Button botaoCentDir;
    private ArrayList<ImagemComponente> centrais;
    private ImagemComponente atualCentral;

    //componentes inferiores
    private ImageView inferior;
    private Button botaoInfEsq;
    private Button botaoInfDir;
    private ArrayList<ImagemComponente> inferiores;
    private ImagemComponente atualInferior;

    //booleano para verficar se o jogador ja acertou a compinação
    private boolean acerto;
    //som
    private MediaPlayer soundButton1;

    //controlador estatico
    public static ContoladorJogo CONTROLADOR;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tela_jogo);

        //animação fade in apenas em versões superiores a LOLLIPOP
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Fade entradaS = new Fade();
            entradaS.setDuration(100);
            getWindow().setEnterTransition(entradaS);
            getWindow().setExitTransition(entradaS);
        }

        //Faz com que a tela da aplicação rode apenas no modo LANDSCAPE
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        //som dos botões
        soundButton1 = MediaPlayer.create(this, R.raw.botao_sound_1);

        //inicia os elementos superiores
        superior = findViewById(R.id.superior);
        botaoSupEsq = findViewById(R.id.botaoSupEsq);
        botaoSupDir = findViewById(R.id.botaoSupDir);
        superiores = new ArrayList<ImagemComponente>();

        //adiciona a parte superior das imagens no arraylist
        superiores.add(new ImagemComponente(superiores.size(), R.drawable.lua_terra_pt0));
        superiores.add(new ImagemComponente(superiores.size(), R.drawable.marte_pt0));
        superiores.add(new ImagemComponente(superiores.size(), R.drawable.mercurio_pt0));
        superiores.add(new ImagemComponente(superiores.size(), R.drawable.netuno_pt0));
        superiores.add(new ImagemComponente(superiores.size(), R.drawable.plutao_pt0));
        superiores.add(new ImagemComponente(superiores.size(), R.drawable.saturno_pt0));
        superiores.add(new ImagemComponente(superiores.size(), R.drawable.sol_pt0));
        superiores.add(new ImagemComponente(superiores.size(), R.drawable.terra_pt0));
        superiores.add(new ImagemComponente(superiores.size(), R.drawable.urano_pt0));
        superiores.add(new ImagemComponente(superiores.size(), R.drawable.venus_pt0));
        superiores.add(new ImagemComponente(superiores.size(), R.drawable.jupiter_pt0));

        //inicia os elementos centrais
        central = findViewById(R.id.central);
        botaoCentEsq = findViewById(R.id.botaoCentEsq);
        botaoCentDir = findViewById(R.id.botaoCentDir);
        centrais = new ArrayList<ImagemComponente>();

        //adiciona a parte central das imagens no arraylist
        centrais.add(new ImagemComponente(centrais.size(), R.drawable.lua_terra_pt1));
        centrais.add(new ImagemComponente(centrais.size(), R.drawable.marte_pt1));
        centrais.add(new ImagemComponente(centrais.size(), R.drawable.mercurio_pt1));
        centrais.add(new ImagemComponente(centrais.size(), R.drawable.netuno_pt1));
        centrais.add(new ImagemComponente(centrais.size(), R.drawable.plutao_pt1));
        centrais.add(new ImagemComponente(centrais.size(), R.drawable.saturno_pt1));
        centrais.add(new ImagemComponente(centrais.size(), R.drawable.sol_pt1));
        centrais.add(new ImagemComponente(centrais.size(), R.drawable.terra_pt1));
        centrais.add(new ImagemComponente(centrais.size(), R.drawable.urano_pt1));
        centrais.add(new ImagemComponente(centrais.size(), R.drawable.venus_pt1));
        centrais.add(new ImagemComponente(centrais.size(), R.drawable.jupiter_pt1));

        //inicia os elementos inferiores
        inferior = findViewById(R.id.inferior);
        botaoInfEsq = findViewById(R.id.botaoInfEsq);
        botaoInfDir = findViewById(R.id.botaoInfDir);
        inferiores = new ArrayList<ImagemComponente>();

        //adiciona a parte inferior das imagens no arraylist
        inferiores.add(new ImagemComponente(inferiores.size(), R.drawable.lua_terra_pt2));
        inferiores.add(new ImagemComponente(inferiores.size(), R.drawable.marte_pt2));
        inferiores.add(new ImagemComponente(inferiores.size(), R.drawable.mercurio_pt2));
        inferiores.add(new ImagemComponente(inferiores.size(), R.drawable.netuno_pt2));
        inferiores.add(new ImagemComponente(inferiores.size(), R.drawable.plutao_pt2));
        inferiores.add(new ImagemComponente(inferiores.size(), R.drawable.saturno_pt2));
        inferiores.add(new ImagemComponente(inferiores.size(), R.drawable.sol_pt2));
        inferiores.add(new ImagemComponente(inferiores.size(), R.drawable.terra_pt2));
        inferiores.add(new ImagemComponente(inferiores.size(), R.drawable.urano_pt2));
        inferiores.add(new ImagemComponente(inferiores.size(), R.drawable.venus_pt2));
        inferiores.add(new ImagemComponente(inferiores.size(), R.drawable.jupiter_pt2));

        //inicia o contralador da lógica do jogo
        CONTROLADOR = new ContoladorJogo(atualSuperior, atualCentral, atualInferior, getResources());

        //array list para configuração padão dos botões
        /*
        ArrayList<Button> BOTOES = new ArrayList<Button>();
        BOTOES.add(botaoSupDir);
        BOTOES.add(botaoSupEsq);
        BOTOES.add(botaoCentDir);
        BOTOES.add(botaoCentEsq);
        BOTOES.add(botaoInfDir);
        BOTOES.add(botaoInfEsq);
*/
        //chamada do método de configuração
        //mudarBotoes(BOTOES);

        //ação dos botões superiores
        botaoSupEsq.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //botaoSupEsq.setBackgroundResource(R.drawable.normal_button_red);

                if (acerto) {
                    atualSuperior = botaoEsquerdo(superiores, superior, atualSuperior);
                    playButton();
                    verificar();

                }

            }
        });

        botaoSupDir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //botaoSupDir.setBackgroundResource(R.drawable.normal_button_red);

                if (acerto) {
                    atualSuperior = botaoDireito(superiores, superior, atualSuperior);
                    playButton();
                    verificar();
                }
            }
        });

        //ação dos botões centrais
        botaoCentEsq.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //botaoCentEsq.setBackgroundResource(R.drawable.normal_button_red);

                if (acerto) {
                    atualCentral = botaoEsquerdo(centrais, central, atualCentral);
                    playButton();
                    verificar();
                }
            }
        });

        botaoCentDir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //botaoCentDir.setBackgroundResource(R.drawable.normal_button_red);

                if (acerto) {
                    atualCentral = botaoDireito(centrais, central, atualCentral);
                    playButton();
                    verificar();
                }
            }
        });

        //ação dos botões inferiores
        botaoInfEsq.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //botaoInfEsq.setBackgroundResource(R.drawable.normal_button_red);

                if (acerto) {
                    atualInferior = botaoEsquerdo(inferiores, inferior, atualInferior);
                    playButton();
                    verificar();
                }
            }
        });

        botaoInfDir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //botaoInfDir.setBackgroundResource(R.drawable.normal_button_red);

                if (acerto) {
                    atualInferior = botaoDireito(inferiores, inferior, atualInferior);
                    playButton();
                    verificar();
                }
            }
        });
    }

    //atualiza o controlador para as imagens atuais nos arrays
    public void atualizarControlador() {
        CONTROLADOR.setImgSup(atualSuperior);
        CONTROLADOR.setImgCent(atualCentral);
        CONTROLADOR.setImgInf(atualInferior);
    }

    //atualiza as imagens pelos botões direitos
    public ImagemComponente botaoDireito(ArrayList<ImagemComponente> imagens, ImageView imagem, ImagemComponente atual) {

        //altera o contador
        atual.addContador();
        if (atual.getContador() >= imagens.size()) {
            atual.setContador(0);
        } else if (atual.getContador() < 0) {
            atual.setContador(imagens.size() - 1);
        }
        //usa a blibioteca glide para renderizar a imagem para a imagemView
        Glide.with(getApplicationContext()).asDrawable().load(imagens.get(atual.getContador()).getImagem()).into(imagem);
        int temp = atual.getContador();
        atual = imagens.get(temp);
        atual.setContador(temp);
        return atual;
    }

    //atualiza as imagens pelos botões esquerdos
    public ImagemComponente botaoEsquerdo(ArrayList<ImagemComponente> imagens, ImageView imagem, ImagemComponente atual) {

        //altera o contador
        atual.removeContador();
        if (atual.getContador() >= imagens.size()) {
            atual.setContador(0);
        } else if (atual.getContador() < 0) {
            atual.setContador(imagens.size() - 1);
        }
        //usa a blibioteca glide para renderizar a imagem para a imagemView
        Glide.with(getApplicationContext()).asDrawable().load(imagens.get(atual.getContador()).getImagem()).into(imagem);
        int temp = atual.getContador();
        atual = imagens.get(temp);
        atual.setContador(temp);

        return atual;
    }

    //verifica se a combinação das imagens está correta
    public void verificar() {

        atualizarControlador();

        //chama o metodo que verfica as posições do controlador e se iguais, inicia a AcertoActivity
        if (CONTROLADOR.vefificarPosicoes()) {

            //impossibilita apertar novamente os botões
            acerto = false;

            //passa as informações do planeta combinado
            Intent intent = new Intent(getApplicationContext(), AcertoActivity.class);
            intent.putExtra("completa", CONTROLADOR.getCompleta());
            intent.putExtra("gif", CONTROLADOR.getGif());
            intent.putExtra("corFundo", CONTROLADOR.getCorFundo());

            //TODO rever isso
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);

            //animação de trasição apenas a versões lollipop e superiores
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(this);
                startActivity(intent, options.toBundle());

            } else {
                startActivity(intent);
            }
        }
    }


    //metodo que embaralha os arraylist
    public void embaralhar() {
        //embaralha as imagens superiores
        Collections.shuffle(superiores);
        Glide.with(getApplicationContext()).asDrawable().load(superiores.get(0).getImagem()).into(superior);
        atualSuperior = superiores.get(0);

        //embaralha as imagens centrais
        Collections.shuffle(centrais);
        Glide.with(getApplicationContext()).asDrawable().load(centrais.get(0).getImagem()).into(central);
        atualCentral = centrais.get(0);

        //embaralha as imagens inferiores
        Collections.shuffle(inferiores);
        Glide.with(getApplicationContext()).asDrawable().load(inferiores.get(0).getImagem()).into(inferior);
        atualInferior = inferiores.get(0);

        //atualiza
        atualizarControlador();

        //chamada recursiva que garante que pelo menos duas atuais não são iguais após embaralhar
        if (CONTROLADOR.verificarDuasPosicoes()) {
            embaralhar();
        }

    }

    private void playButton(){
        //se o som do botão estiver tocando para e toca novamente
        try {
            if (soundButton1.isPlaying()) {
                soundButton1.stop();
                soundButton1.release();
                soundButton1 = MediaPlayer.create(TelaJogoActivity.this, R.raw.botao_sound_1);
            }
            soundButton1.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    //inicia o servico de som de fundo e embaralha os arrays e habilita o jogo
    @Override
    protected void onResume() {
        stopService(new Intent(this, BackgroundSoundServiceAcertoActivity.class));
        super.onResume();
        startService(new Intent(TelaJogoActivity.this, BackgroundSoundServiceTelaJogo.class));
        embaralhar();
        acerto = true;
    }

    //para o servico de som de fundo
    protected void onDestroy() {
        super.onDestroy();
        stopService(new Intent(TelaJogoActivity.this, BackgroundSoundServiceTelaJogo.class));
    }

    //pausa o servico de som de fundo
    protected void onPause() {
        super.onPause();
        stopService(new Intent(TelaJogoActivity.this, BackgroundSoundServiceTelaJogo.class));
    }
}




